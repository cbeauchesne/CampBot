# CampBot

[![PyPI version](https://badge.fury.io/py/campbot.svg)](https://pypi.org/project/campbot/) 
[![Build](https://gitlab.com/cbeauchesne/CampBot/badges/master/build.svg)](https://gitlab.com/cbeauchesne/CampBot/pipelines) 
[![coverage report](https://gitlab.com/cbeauchesne/CampBot/badges/master/coverage.svg)](https://cbeauchesne.gitlab.io/CampBot/htmlcov-3.7) 
[![Documentation Status](https://readthedocs.org/projects/campbot/badge/?version=latest)](https://campbot.readthedocs.io/en/latest/?badge=latest)
 

Bot framework for [camptocamp.org](https://www.camptocamp.org/)

## Installation

```batch
pip install campbot
```

## Documentation

[Read the docs!](https://campbot.readthedocs.io/)